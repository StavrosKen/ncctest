'use strict';


var apiURL = "http://NCCTest.local/api"


angular.module('NCCTest', ['ngRoute','restangular'])

.config(['$routeProvider', 'RestangularProvider', 
	function($routeProvider, RestangularProvider) {
   $routeProvider
            .when('/', {
                controller: 'UserManagementCtrl',
                templateUrl: 'TableTestModule/views/UserManagement.html'
            })
            .otherwise({
                redirectTo: '/'
            });


        RestangularProvider.setBaseUrl(apiURL);
        RestangularProvider.setDefaultHttpFields({
            timeout: 30000
        });

}])


.run(['UserSync',function(UserSync){

	//instantiate UserSync

}]);
