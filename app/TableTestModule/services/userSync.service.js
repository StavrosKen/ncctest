angular.module('NCCTest')

.factory('UserSync', ['$q', '$rootScope', 'Restangular',
    function($q, $rootScope, Restangular) {

        var users = undefined;

        var getUsersList = function(){

            var deferred = $q.defer();
            
            Restangular.all('users.php').customGET("")
                .then(function(response){

               users = response.data;

            },function(){

                console.log("couldnt fetch users");
                deferred.reject(false);
            });       

            return deferred.promise;
        }


        return {

            init: (function() {

                var deferred = $q.defer();

                getUsersList().then(function(){

                        deferred.resolve("users fetched");

                    },function(){

                        deferred.reject("problem fetching users");
                    });

                    return deferred.promise;
                }()),
            
            allUsers: function(){

                if (users){
                    return users;
                }  
            },

            insertUser:function(user){

                if(users.push(user)){
                    //send a PUT request to an api to store the new user to DB
                    return "User added successfully!";
                }
                
            }

        }

    }]);