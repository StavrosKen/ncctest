angular.module('NCCTest')

    .directive('userTable', ['UserSync', '$rootScope', function(UserSync, $rootScope){
        return {
            restrict: 'A',
            templateUrl: "TableTestModule/directives/UserTable/userTableView.html",
            link: function($scope, $element, $attrs){

            	$scope.users = UserSync.allUsers();
            	$scope.page = 1;
            	$scope.resultsPerPage = $attrs.perpage ? $attrs.perpage : 15;
            	$scope.numberOfPages = Math.ceil($scope.users.length / $scope.resultsPerPage);
            	$scope.usersPage = $scope.users.slice(($scope.page-1)*$scope.resultsPerPage, $scope.page*$scope.resultsPerPage);
            	$scope.sortExpr = "name";
                
                $rootScope.$on("users-updated", function(){
                    $scope.users = UserSync.allUsers();

                });

                $scope.search = function (row) {
        			return (angular.lowercase(row.name).indexOf(angular.lowercase($scope.query) || '') !== -1 ||
                			angular.lowercase(row.email).indexOf(angular.lowercase($scope.query) || '') !== -1);
    			};

    			$scope.changeSort = function(sortBy){
                    if($scope.sortExpr == sortBy){
                        sortBy = "-"+sortBy;
                    }
    				$scope.sortExpr = sortBy;
    			};

    			$scope.changePage = function(page){
    				$scope.page = page;
    				$scope.usersPage = $scope.users.slice(($scope.page-1)*$scope.resultsPerPage, $scope.page*$scope.resultsPerPage);
    			};

    			$scope.getNumber = function(num) {
    				return new Array(num);   
				};
            }
        }
    }]);