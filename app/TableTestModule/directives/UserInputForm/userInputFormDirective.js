angular.module('NCCTest')

    .directive('userInputForm', ['UserSync', '$rootScope', function(UserSync, $rootScope){
        return {
            restrict: 'A',
            templateUrl: "TableTestModule/directives/UserInputForm/userInputFormView.html",
            link: function($scope, $element, $attrs){

            	$scope.newUser = undefined;
                $scope.insertUser = function(){

                    $scope.message = UserSync.insertUser($scope.newUser);
                    $rootScope.$broadcast('users-updated');
                    $scope.newUser = undefined;
                }
            }
        }
    }]);