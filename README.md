# NCC Group AngularJS dev test
completed by Stavros Kainich.

This project is an application that manipulates user data.

It fetches the data from php (currently mocked) and populates a table in the frontend.
The table is paginated, supports orderby any column (ascending or descending) and supports
filter functionality on the name and email fields.

It is well structured and bundled in one module.
It also uses:

1. a Factory that handles the synchronization of the data
2. a Form directive
3. a Table directive


To install, please change the apiURL in app.js to match the virtual host you will use 
to test the app.
Alternatively just name your virtual host NCCTest.local.


The code was completed in 2 hours including the search for a solid foundation to build the app on.